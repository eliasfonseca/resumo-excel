<?php 

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$disputa = [
	'ano'				=> '2020',
	'sequencial'		=> '12',
	'dtPublicacao'		=> '19/01/2020 - 08:00',
	'dtAbertura' 		=> '19/01/2020 - 09:00',
	'dtEncerramento' 	=> '25/01/2020 - 18:00',
	'tempoExtra' 		=> '02:00',
	'segmento' 			=> 'TRANSPORTE, ARMAZENAGEM E CORREIO',
	'categoria' 		=> 'TRANSPORTE TERRESTRE',
	'objeto' 			=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
	ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
	voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
];

$lances = [
	'0'	=>	[
		'interessado'	=> 'rapidaobahia',
		'cnpj'			=> '45.325.238/0001-25',
		'dataHora'		=> '19/01/2020 11:37:01',
		'valor'			=> 'R$ 1.333,00',
		'classificacao'	=> '1º'
	],
	'1'	=>	[
		'interessado'	=> 'rapidex',
		'cnpj'			=> '58.325.138/0001-75',
		'dataHora'		=> '11/01/2020 01:37:08',
		'valor'			=> 'R$ 1.893,00',
		'classificacao'	=> '2º'
	],
	'2'	=>	[
		'interessado'	=> 'aquajato',
		'cnpj'			=> '42.341.638/0001-35',
		'dataHora'		=> '30/01/2020 10:07:41',
		'valor'			=> 'R$ 2.485,00',
		'classificacao'	=> '3º'
	],
	
];


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$spreadsheet->getProperties()
    ->setCreator("Correios")
    ->setLastModifiedBy("Correios")
    ->setTitle("Relatorio - Disputa Encerrada")
    ->setSubject("Relatorio - Disputa Encerrada")
    ->setDescription(
        "Relatorio - Disputa Encerrada."
    )
    ->setKeywords("Relatorio - Disputa Encerrada")
    ->setCategory("E-Disputa");
$spreadsheet->setActiveSheetIndex(0);

// Estilos
$estiloBordas = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => 'e1e4e5'],
        ],
    ],
];

$estiloCabecalho = [
	'font' => [
		'size' => 14,
        'bold' => true,
        'color' => ['argb' => 'ffffff']
    ]
];

$estiloVencedor = [
    'font' => [
        'bold' => true,
        'color' => ['argb' => '0210ff']
    ],
];

$estiloCabecalhoLances = [
    'font' => [
        'bold' => true
    ],
];

$sheet->setCellValue('A1', 'Resumo da Disputa - N° '.$disputa['ano'].'/'.$disputa['sequencial']);

$sheet->setCellValue('A2', 'Data/Hora Publicação:');
$sheet->setCellValue('B2', $disputa['dtPublicacao']);
$sheet->setCellValue('A3', 'Data/Hora Abertura:');
$sheet->setCellValue('B3', $disputa['dtAbertura']);
$sheet->setCellValue('A4', 'Data/Hora Encerramento:');
$sheet->setCellValue('B4', $disputa['dtEncerramento']);
$sheet->setCellValue('A5', 'Tempo Extra:');
$sheet->setCellValue('B5', $disputa['tempoExtra']);
$sheet->setCellValue('A6', 'Segmento:');
$sheet->setCellValue('B6', $disputa['segmento']);
$sheet->setCellValue('A7', 'Categoria:');
$sheet->setCellValue('B7', $disputa['categoria']);
$sheet->setCellValue('A8', 'Objeto da disputa:');
$sheet->setCellValue('B8', $disputa['objeto']);

$sheet->setCellValue('A9', '');

//Cabeçalho dos lances
$qtdLances = count($lances);
$txtLance = ($qtdLances >= 2) ? 'lances' : 'lance';
$sheet->setCellValue('A10', 'Lances registrados pelos Interessados ('.$qtdLances.' '.$txtLance.')');
$sheet->setCellValue('A11', 'Interessado');
$sheet->setCellValue('B11', 'CNPJ');
$sheet->setCellValue('C11', 'Data/Hora');
$sheet->setCellValue('D11', 'Valor do Lance');
$sheet->setCellValue('E11', 'Classificação');

$spreadsheet->getActiveSheet()->getStyle('A11:E11')->applyFromArray($estiloCabecalhoLances);

$row = 12;
for ($i=0; $i < $qtdLances; $i++) {
	if ($i == 0) {
		$spreadsheet->getActiveSheet()->getStyle('A12:E12')->applyFromArray($estiloVencedor);
	}

	$sheet->setCellValue('A'.$row, $lances[$i]['interessado']);
	$sheet->setCellValue('B'.$row, $lances[$i]['cnpj']);
	$sheet->setCellValue('C'.$row, $lances[$i]['dataHora']);
	$sheet->setCellValue('D'.$row, $lances[$i]['valor']);
	$sheet->setCellValue('E'.$row, $lances[$i]['classificacao']);

	$spreadsheet->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($estiloBordas);

	$row = ++$row;
}

//Configura o cabeçalho usando o estilo estiloCabecalho
$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($estiloCabecalho);

//Define a cor de fundo da célula
$spreadsheet->getActiveSheet()->getStyle('A1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('337ab7');

$spreadsheet->getActiveSheet()->getStyle('A9:E9')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('ffffff');

$spreadsheet->getActiveSheet()->getStyle('A2:A8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('dadada');

$spreadsheet->getActiveSheet()->getStyle('B2:B8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('f9f9f9');

$spreadsheet->getActiveSheet()->getStyle('A11:E11')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('dadada');

//Define a quebra de texto na célula
$spreadsheet->getActiveSheet()->getStyle('B8')
    ->getAlignment()->setWrapText(true);

//Alinhamento horizontal e vertical nas células
$spreadsheet->getActiveSheet()->getStyle('A1')
    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$spreadsheet->getActiveSheet()->getStyle('A11:E11')
    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$spreadsheet->getActiveSheet()->getStyle('E')
    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$spreadsheet->getActiveSheet()->getStyle('A1')
    ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$spreadsheet->getActiveSheet()->getStyle('A10')
    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

$spreadsheet->getActiveSheet()->getStyle('A8')
    ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

$spreadsheet->getActiveSheet()->getStyle('B8')
    ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

$spreadsheet->getActiveSheet()->getStyle('E12')
    ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

//Insere bordas no rodapé da célula, num intervalo específicado
for ($i=2; $i < 9; $i++) {
	$spreadsheet->getActiveSheet()->getStyle('A'.$i.':E'.$i)->applyFromArray($estiloBordas);
}

$spreadsheet->getActiveSheet()->getStyle('A10:E10')->applyFromArray($estiloBordas);


//Mesclagem de células
$spreadsheet->getActiveSheet()->mergeCells('A1:E1');
$spreadsheet->getActiveSheet()->mergeCells('B2:E2');
$spreadsheet->getActiveSheet()->mergeCells('B3:E3');
$spreadsheet->getActiveSheet()->mergeCells('B4:E4');
$spreadsheet->getActiveSheet()->mergeCells('B5:E5');
$spreadsheet->getActiveSheet()->mergeCells('B6:E6');
$spreadsheet->getActiveSheet()->mergeCells('B7:E7');
$spreadsheet->getActiveSheet()->mergeCells('B8:E8');
$spreadsheet->getActiveSheet()->mergeCells('A10:E10');

//Definição de altura da linha
$spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(70);

//Largura das colunas
$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(25);
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);

//Adicionar uma imagem a uma planilha
// $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
// $drawing->setName('Logo');
// $drawing->setDescription('Correios');
// $drawing->setPath('img/logo.png');
// $drawing->setHeight(36);

$dataAgora = date('d-m-Y');
$nomeArquivo = 'Relatório-'.$dataAgora;

// $writer = new Xlsx($spreadsheet);
// $writer->save($nomeArquivo.'.xlsx');

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="myfile.xlsx"');
header('Cache-Control: max-age=0');

$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
